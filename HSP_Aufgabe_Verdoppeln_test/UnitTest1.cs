using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Verdoppeln1;

namespace HSP_Aufgabe_Verdoppeln_test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Verdoppeln_Test1()
        {
            Verdoppeln prg = new Verdoppeln();

            var verdoppeln = prg.Verdoppeln_input(1);

            Assert.AreEqual("z: 2\nz: 4\nz: 8\nz: 16\nz: 32\nz: 64\nhui...\nz: 128\nhui...\nSteps: 7", verdoppeln);
        }

        [TestMethod]
        public void Verdoppeln_Test2()
        {
            Verdoppeln prg = new Verdoppeln();

            var verdoppeln = prg.Verdoppeln_input(2);

            Assert.AreEqual("z: 4\nz: 8\nz: 16\nz: 32\nz: 64\nhui...\nz: 128\nhui...\nSteps: 6", verdoppeln);
        }

        [TestMethod]
        public void Verdoppeln_Test3()
        {
            Verdoppeln prg = new Verdoppeln();

            var verdoppeln = prg.Verdoppeln_input(3);

            Assert.AreEqual("z: 6\nz: 12\nz: 24\nz: 48\nhui...\nz: 96\nhui...\nz: 192\nhui...\nSteps: 6", verdoppeln);
        }
    }
}
