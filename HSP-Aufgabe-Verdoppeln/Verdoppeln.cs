﻿//*******************************************Aufgabenstellung*******************************************************************************
//
//Es soll die Methode "Verdoppeln_input()" so erweitert werden, dass sie eine ihr übergebene Zahl (int input) so lange verdoppelt, bis diese
//dreistellig wird. Jedes Zwischenergebnis soll dabei in einer eigenen Zeile ausgegeben werden. Implementieren Sie zusätzlich folgendes:
//
//  - Die Anzahl der erforderlichen Verdopplungen soll am Ende der Programmausgaben ebefalls ausgegeben werden
//  - Ist die Differenz eines Zwischenergebnisses zum vorherigen Zwischenergebnis größer als 20, soll in einer eigenen Zeile
//    die Meldung "hui..." ausgegeben werden
//
//*******************************************Tipps & Hinweise*******************************************************************************
//
//  - Damit das UnitTesting fehlerfrei funktioniert, müssen Sie eine bestimmte Formatierung einhalten
//       -> Bauen Sie Ihren Ausgabe-String also bitte wie folgt auf:    (am Beispiel der Verdopplung der Zahl 4)
//          
//       z: 8
//       z: 16
//       z: 32
//       z: 64
//       hui...
//       z: 128
//       hui...
//       Steps: 5
//
//  - achten Sie bei der Formatierung bitte genau auf Leerzeichen, Groß-/Kleinschreibung, Umbrüche, etc. (wenn nicht, wird das Programm zwar
//    funktionieren, die UnitTests aber nicht... )
//  - für einen Zeilenumbruch innerhalb eines Strings verwenden Sie die Zeichenfolge "\n"
//    Beispiel:  Der string "Eine Zeile\nmehr bitte" wird so ausgegeben: Eine Zeile
//                                                                       mehr bitte     <- einfach mal ausprobieren!

using System;

//Benennen des genutzten Namesraumes (Namespaces)
namespace Verdoppeln1
{
    public class Verdoppeln  //Deklaration der Klasse "Verdoppeln" (-> OOP)
    {

        static void Main(string[] args)                         //Hauptprogramm
        {
            Verdoppeln prg = new Verdoppeln();                  // Erstellen eines Objektes der Klasse "Verdoppeln"... also quasi ein Aufruf des Programms (-> OOP)
            Console.Write("Bitte geben Sie eine einstellige ganze Zahl ein, die Sie verdoppeln möchten:\nz: ");  
                                                                // Textausgabe auf der Konsole. Aufforderung zur Eingabe einer Zahl
            int input = Convert.ToInt32(Console.ReadLine());    // Deklaration und Zuweisung der Variable "input". Die Usereingabe wird zur sicherheit in den Datentyp int konvertiert und dann in der Variable gespeichert
            string doppelzahlen = prg.Verdoppeln_input(input);  // Der string "doppelzahlen" wird deklariert. In ihm wird das Ergebnis der Methode "Verdoppeln_input()" gespeichert, welcher wiederum der Parameter "input" übergeben wird 
            Console.WriteLine(doppelzahlen);                    // Ausgabe des strings "doppenzahlen" in der Konsole

            Console.ReadKey();                                  // Warten auf Usereingabe zum Beenden des Programms
        }

        /// <summary>
        /// Verdoppelt den eingegebenen Parameter solange, bis die Zahl dreistellig wird.
        /// </summary>
        /// <param name="input">Zahl, welche mehrfach verdoppelt wird.</param>
        /// <returns>Die mehrfach verdoppelte Zahl < 1000 </returns>
        public string Verdoppeln_input(int input)   //Deklaration der Methode "Verdoppeln_input". Sie gibt einen string zurück und erwartet einen integer
        {
            //Variablen (noch nicht alle vorhanden... ist ja klar)
            string doppelzahlen = "";               //Deklaration des strings "doppelzahlen", der vorerst mit 'nichts' gefüllt wird...

            // <Ihr Code hier... >

            return doppelzahlen;                    //Rückgabe des strings - reminder: Der wird im Hauptprogramm ausgegeben!
        }
    }
}
