# Verdoppeln

Aufgabe Verdoppeln für Hochsprachenprogrammierung / Mecke an der Jade Hochschule

Aufgabe:  Es soll die Methode "Verdoppeln_input()" so erweitert werden, dass sie eine ihr übergebene Zahl (int input) so lange verdoppelt, bis diese
dreistellig wird. Jedes Zwischenergebnis soll dabei in einer eigenen Zeile ausgegeben werden. Implementieren Sie zusätzlich folgendes:

* Die Anzahl der erforderlichen Verdopplungen soll am Ende der Programmausgaben ebefalls ausgegeben werden
* Ist die Differenz eines Zwischenergebnisses zum vorherigen Zwischenergebnis größer als 20, soll in einer eigenen Zeile
  die Meldung "hui..." ausgegeben werden

Weitere Tipps finden Sie in den Kommentaren im Code!

Wenn Sie fertig sind, dann
* Im Projektmappen Explorer (engl. Solution Explorer) klicken Sie rechts das -Unittesting Projekt und wählen Sie "Tests ausführen"
* Starten Sie die Tests innerhalb des Test-Explorers

Diesen Ablauf können Sie auch mit Hilfe des Videos "Aufgabensammlung Einstieg" nachvollziehen
